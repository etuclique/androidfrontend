package com.example.user.todolist;

/**
 * Created by İlayda Şahiner on 15.07.2017.
 */

public class Issue {

    User user;
    int id;
    String title;
    String content;
    String dueDate;
    String created_at;
    Boolean isItDone;
    Boolean overDue;


    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public Boolean getIsItDone() {
        return isItDone;
    }

    public void setIsItDone(Boolean isItDone) {
        this.isItDone = isItDone;
    }

    public Boolean getOverDue() {
        return overDue;
    }

    public void setOverDue(Boolean overDue) {
        this.overDue = overDue;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
