package com.example.user.todolist;

import android.graphics.Bitmap;

import java.util.List;

/**
 * Created by İlayda Şahiner on 15.07.2017.
 */

public class User {

        int id;
        String adiSoyadi;
        String kullaniciAdi;
        Bitmap resim;
        String email;
        List<Issue>  issueList;


        public int getId() {return id;}

        public void setId(int id) {
            this.id = id;
        }

        public String getAdiSoyadi() {
            return adiSoyadi;
        }

        public void setAdiSoyadi(String adiSoyadi) {
            this.adiSoyadi = adiSoyadi;
        }

        public String getKullaniciAdi() {
            return kullaniciAdi;
        }

        public void setKullaniciAdi(String kullaniciAdi) {
            this.kullaniciAdi = kullaniciAdi;
        }

        public Bitmap getResim() {
            return resim;
        }

        public void setResim(Bitmap resim) {
            this.resim = resim;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public List<Issue> getIssueList() {
            return issueList;
    }

         public void setIssueList(List<Issue> issueList) {
            this.issueList = issueList;
    }
}

