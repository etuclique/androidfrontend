package com.example.user.todolist;

/**
 * Created by İlayda Şahiner on 13.07.2017.
 */

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import java.util.ArrayList;

public class Fragment_BuHaftaYapilacaklar extends ListFragment {
    private static String urlBuHafta = "https://etucliquetodo.herokuapp.com/api/v1/users/1/issues/buHafta";
    ArrayList<Issue> issuelar;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        issuelar = new ArrayList<Issue>();
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_buhafta, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        new GetBuHaftaYapilacaklarList().execute();
    }

    private class GetBuHaftaYapilacaklarList extends AsyncTask<String, Void, String> {
        ArrayList<Issue> issuelar;

        @Override
        protected String doInBackground(String... arg0) {

            WebRequest webreq = new WebRequest();
            String jsonStr = null;
            try {
                jsonStr = webreq.makeWebServiceCallGet(urlBuHafta);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.i(jsonStr, "!! Json");

            issuelar = ParseJsonForIssue(jsonStr);

            return "a";

        }


        @Override
        protected void onPostExecute(String requestresult) {
            super.onPostExecute(requestresult);
            if (issuelar == null) {
                Log.i("bosmu", "Issuelar arrayi boş");
            } else {
                IssuelarListesiAdapter adapter = new IssuelarListesiAdapter(getActivity(), issuelar);
                setListAdapter(adapter);
                //setRetainInstance(true);
            }
        }
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        Fragment fragment_ayrinti= new Fragment_IssueAyrinti();
        Bundle bundle = new Bundle();

        bundle.putInt("id_of_clicked_issue",issuelar.get(position).getId());
        fragment_ayrinti.setArguments(bundle);
        FragmentTransaction trans = getFragmentManager().beginTransaction();
        trans.replace(R.id.fragment1container, fragment_ayrinti,"fragment_ayrinti");
        trans.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        trans.addToBackStack(null);
        trans.commit();
    }

        private ArrayList<Issue> ParseJsonForIssue(String json) {
            //ArrayList<Issue> issuelar = new ArrayList<Issue>();
            if (json != null) {
                try {
                    JSONArray jsonObj = new JSONArray(json);
                    for (int i = 0; i < jsonObj.length(); i++) {
                        JSONObject issueObj = jsonObj.getJSONObject(i);
                        Issue issue = new Issue();
                        issue.setId(issueObj.getInt("id"));
                        issue.setTitle(issueObj.getString("title"));
                        issue.setContent(issueObj.getString("content"));
                        issue.setDueDate(issueObj.getString("dueDate"));
                        issue.setCreated_at(issueObj.getString("created_at"));
                        /*issue.setIsItDone(issueObj.getBoolean("isItDone"));
                        issue.setOverDue(issueObj.getBoolean("overDue"));*/
                        User user = new User();
                        user.setId(issueObj.getInt("user_id"));
                        issuelar.add(issue);
                    }
                } catch (JSONException e) {
                    Log.e("MYAPP", "unexpected JSON exception", e);
                    // Do something to recover ... or kill the app.
                }
            }
            return issuelar;
        }
    }









