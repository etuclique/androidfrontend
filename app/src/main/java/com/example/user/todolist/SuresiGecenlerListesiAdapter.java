package com.example.user.todolist;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by İlayda Şahiner on 17.07.2017.
 */

public class SuresiGecenlerListesiAdapter extends BaseAdapter{
    ArrayList<Issue> issuelar;
    LayoutInflater layoutInflater;
    Activity activity;

    public SuresiGecenlerListesiAdapter(Activity activity, ArrayList<Issue> mlist) {
        layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.activity = activity;
        issuelar = mlist;
    }
    @Override
    public int getCount() {
        return issuelar.size();
    }

    @Override
    public Object getItem(int position) {
        return issuelar.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View satirView;
        satirView = layoutInflater.inflate(R.layout.satir_for_suresigecenler, null);
        ImageView image = (ImageView) satirView.findViewById(R.id.dosyaturu);
        TextView title =(TextView) satirView.findViewById(R.id.title);
        TextView dueDate = (TextView) satirView.findViewById(R.id.duedateinfo);
        TextView content = (TextView) satirView.findViewById(R.id.contentinfo);
        TextView createdat = (TextView) satirView.findViewById(R.id.createdatinfo);
        final Issue issue = issuelar.get(position);

        image.setImageResource(R.drawable.note);
        title.setText(issue.getTitle());
        dueDate.setText(issue.getDueDate());
        content.setText(issue.getContent());
        createdat.setText(issue.getCreated_at());
        return satirView;
    }
}
