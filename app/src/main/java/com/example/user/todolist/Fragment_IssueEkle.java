package com.example.user.todolist;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;

import java.io.IOException;
import java.util.Calendar;

/**
 * Created by İlayda Şahiner on 18.07.2017.
 */

public class Fragment_IssueEkle extends Fragment {

    private static String urlIssueEkleme = "https://etucliquetodo.herokuapp.com/api/v1/users/1/issues/";
    String paramsForIssueCreation="";
    static TextView tdate;
    static TextView ttime;
    EditText issuetitle;
    EditText issuecontent;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_ekle, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        issuetitle = (EditText) view.findViewById(R.id.issuetitle);
        issuecontent = (EditText) view.findViewById(R.id.issuecontent);
        //final EditText issuedueDate = (EditText) view.findViewById(R.id.issueduedate);
        Button buttontime = (Button) view.findViewById(R.id.time);
        Button buttondate = (Button) view.findViewById(R.id.date);
        Button buttonekle = (Button) view.findViewById(R.id.buttonekle);
        tdate=(TextView) view.findViewById(R.id.datetext);
        ttime=(TextView) getView().findViewById(R.id.timetext);
        buttontime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment newFragment = new TimePickerFragment();
                newFragment.show(getFragmentManager(), "timePicker");
            }
        });

        buttondate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment newFragment = new DatePickerFragment();
                newFragment.show(getFragmentManager(), "datePicker");
            }
        });

        buttonekle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String title = issuetitle.getText().toString();
                String content = issuecontent.getText().toString();
                String time = ttime.getText().toString();
                String date = tdate.getText().toString();
                paramsForIssueCreation = "title=" + title + "&" + "content=" +content + "&" +"dueDate=" +date+" "+time;
                System.out.println(paramsForIssueCreation);
                new IssueEkle().execute();
            }
        });




    }
    private class IssueEkle extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... arg0) {

            WebRequest webreq = new WebRequest();
            String jsonStr = "";
            try {
                jsonStr = webreq.makeWebServiceCallPost(urlIssueEkleme,paramsForIssueCreation);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.i(String.valueOf(jsonStr),"!! Json");
            return "a";


        }

        @Override
        protected void onPostExecute(String requestresult) {
            super.onPostExecute(requestresult);
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
            alertDialogBuilder.setTitle("Issue Creation");
            alertDialogBuilder.setMessage("Issue başarılı bir şekilde oluşturuldu");
            alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            final AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(DialogInterface arg0) {
                    alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.parseColor("#4C87F4"));
                }
            });
            alertDialog.show();
            issuetitle.setText("");
            issuecontent.setText("");
            ttime.setText("");
            tdate.setText("");

        }
    }


    public static class TimePickerFragment extends DialogFragment
            implements TimePickerDialog.OnTimeSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current time as the default values for the picker
            final Calendar c = Calendar.getInstance();
            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);

            // Create a new instance of TimePickerDialog and return it
            return new TimePickerDialog(getActivity(), this, hour, minute,
                    DateFormat.is24HourFormat(getActivity()));
        }

        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            String time = hourOfDay + ":" + String.valueOf(minute);
            System.out.println(time);
            if (hourOfDay < 10)
                time = "0" + hourOfDay + ":" + String.valueOf(minute);
            if (minute < 10)
                time = String.valueOf(hourOfDay) + ":0" + String.valueOf(minute);
            if(minute < 10 && hourOfDay<10)
                time = "0"+String.valueOf(hourOfDay) + ":0" + String.valueOf(minute);
            ttime.setText(time);
        }
    }

    public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            String date=String.valueOf(year) +"-"+String.valueOf(month+1)
                    +"-"+String.valueOf(day);
            if(month<10)
                date=String.valueOf(year) +"-0"+String.valueOf(month+1)
                    +"-"+String.valueOf(day);
            if(day<10)
                date=String.valueOf(year) +"-"+String.valueOf(month+1)
                        +"-0"+String.valueOf(day);
            if(month<10 && day<10)
                date=String.valueOf(year) +"-0"+String.valueOf(month+1)
                        +"-0"+String.valueOf(day);

            tdate.setText(date);
        }
    }
}


