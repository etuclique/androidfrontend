package com.example.user.todolist;

/**
 * Created by İlayda Şahiner on 10.07.2017.
 */

import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.util.Base64;
import android.view.Gravity;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.support.v4.app.FragmentTransaction;
import android.widget.ImageView;
import android.widget.TextView;


public class ListActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    Toolbar toolbar;
    Dialog issue_ekle_dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);


        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.content_frame, new RootFragment1());
        transaction.commit();

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View headerLayout = navigationView.getHeaderView(0);
        ImageView iv = (ImageView) headerLayout.findViewById(R.id.imageview_bilgi);
        iv.setImageResource(R.drawable.anonim);

        TextView tv1 = (TextView) headerLayout.findViewById(R.id.bilgi_email);
        tv1.setText("ilaydasahiner@hotmail.com");
        TextView tv2 = (TextView) headerLayout.findViewById(R.id.bilgi_kullaniciAdi);
        tv2.setText("ilaydasahiner");

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            /**
             * Called when a drawer has settled in a completely closed state.
             */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            /**
             * Called when a drawer has settled in a completely open state.
             */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };

        mDrawerLayout.addDrawerListener(mDrawerToggle);
        mDrawerLayout.postDelayed(new Runnable() {
            @Override
            public void run() {
                mDrawerLayout.openDrawer(Gravity.LEFT);
            }
        }, 1000);
        mDrawerToggle.syncState();
       /* FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


               *//* issue_ekle_dialog = new Dialog(getApplicationContext());
                issue_ekle_dialog.setContentView(R.layout.dialog_ekle);
                issue_ekle_dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                issue_ekle_dialog.show();
                issue_ekle_dialog.getWindow().setGravity(Gravity.CENTER);*//*

*//*

                final TextView title = (TextView) view.findViewById(R.id.title);
                final EditText content = (EditText) view.findViewById(R.id.content);
                final TextView timeTxt = (TextView) view.findViewById(R.id.timetext);
                final TextView dateTxt = (TextView) view.findViewById(R.id.datetext);
                final Button time = (Button) view.findViewById(R.id.time);
                final Button date = (Button) view.findViewById(R.id.date);

                date.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // Get Current Date

                        final Calendar c = Calendar.getInstance();
                        int mYear = c.get(Calendar.YEAR);
                        int mMonth = c.get(Calendar.MONTH);
                        int mDay = c.get(Calendar.DAY_OF_MONTH);


                        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                                new DatePickerDialog.OnDateSetListener() {

                                    @Override
                                    public void onDateSet(DatePicker view, int year,
                                                          int monthOfYear, int dayOfMonth) {

                                        dateTxt.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                                    }
                                }, mYear, mMonth, mDay);
                        datePickerDialog.show();
                    }

                });




        if (v == btnTimePicker) {

                                       // Get Current Time
                                       final Calendar c = Calendar.getInstance();
                                       mHour = c.get(Calendar.HOUR_OF_DAY);
                                       mMinute = c.get(Calendar.MINUTE);

                                       // Launch Time Picker Dialog
                                       TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                                               new TimePickerDialog.OnTimeSetListener() {

                                                   @Override
                                                   public void onTimeSet(TimePicker view, int hourOfDay,
                                                                         int minute) {

                                                       txtTime.setText(hourOfDay + ":" + minute);
                                                   }
                                               }, mHour, mMinute, false);
                                       timePickerDialog.show();
                                   }
                               }




                DatePickerDialog datePickerDialog = new DatePickerDialog(
                        context, MainActivity.this, startYear, starthMonth, startDay);
                date.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        datePickerDialog.show();
                    }
                });
                //DIALOG_PAYLAS DIALOG'S PAYLAŞ BUTTON
                Button paylas = (Button) issue_ekle_dialog.findViewById(R.id.buttonekle);
                paylas.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String issueTitle = title.getText().toString();
                        String issueContent = content.getText().toString();
                        String issueDate = title.getText().toString();

                        *//*
*//*paramsForEkle = issueTitle + "/" + issueContent + "/" + issueDate;

                        System.out.println(paramsForPaylas);
                        new PaylasTask().execute();*//**//*

                    }
                });
                //DIALOG_PAYLAS DIALOG'S KAPAT BUTTON
                Button kapat = (Button) issue_ekle_dialog.findViewById(R.id.buttonvazgec);
                kapat.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        issue_ekle_dialog.dismiss();
                    }
                });

*//*
            }
        });*/


                navigationView.setNavigationItemSelectedListener(this);


    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
        else{
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

       /* //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
*/
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        int id = item.getItemId();

        if (id == R.id.bu_hafta) {
            toolbar.setTitle(item.getTitle());
            RootFragment1 rootFragment1 = new RootFragment1();
            ft.replace(R.id.content_frame, rootFragment1);
            ft.commit();
        } else if (id == R.id.gelecek_hafta) {
            toolbar.setTitle(item.getTitle());
            RootFragment2 rootFragment2 = new RootFragment2();
            ft.replace(R.id.content_frame, rootFragment2);
            ft.commit();

        } else if (id == R.id.suresi_gecenler) {
            toolbar.setTitle(item.getTitle());
            Fragment_SuresiBitenler fragmentSuresiBitenler = new Fragment_SuresiBitenler();
            ft.replace(R.id.content_frame, fragmentSuresiBitenler);
            ft.commit();

        } else if (id == R.id.tamamlanlar) {
            toolbar.setTitle(item.getTitle());
            Fragment_Tamamlananlar fragmenttamamlananlar = new Fragment_Tamamlananlar();
            ft.replace(R.id.content_frame,fragmenttamamlananlar);
            ft.commit();
        } else if (id == R.id.ekle) {
            toolbar.setTitle(item.getTitle());
            Fragment_IssueEkle rootFragment4 = new Fragment_IssueEkle();
            ft.replace(R.id.content_frame, rootFragment4);
            ft.commit();
        }
            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            drawer.closeDrawer(GravityCompat.START);
            return true;
        }


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    private Bitmap convertBase64toBitmap(String base) {
        int index = base.indexOf(',');
        String base64 = base.substring(index + 1);
        byte[] decodedString = Base64.decode(base64, Base64.DEFAULT);
        Bitmap bitmap = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        return bitmap;
    }
}