package com.example.user.todolist;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by kadercan on 09.08.2017.
 */

public class Fragment_SuresiBitenler extends ListFragment {
        private static String urlSuresiGecenler = "https://etucliquetodo.herokuapp.com/api/v1/users/1/issues/suresiGecenler";
        ArrayList<Issue> issuelar;


        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            issuelar = new ArrayList<Issue>();
            // Inflate the layout for this fragment
            return inflater.inflate(R.layout.fragment_buhafta, container, false);
        }

        @Override
        public void onViewCreated(View view, Bundle savedInstanceState) {
            super.onViewCreated(view, savedInstanceState);
            new GetSuresiGecenler().execute();
        }

        private class GetSuresiGecenler extends AsyncTask<String, Void, String> {
            ArrayList<Issue> issuelar;

            @Override
            protected String doInBackground(String... arg0) {

                WebRequest webreq = new WebRequest();
                String jsonStr = null;
                try {
                    jsonStr = webreq.makeWebServiceCallGet(urlSuresiGecenler);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Log.i(jsonStr, "!! Json");

                issuelar = ParseJsonForIssue(jsonStr);

                return "a";

            }


            @Override
            protected void onPostExecute(String requestresult) {
                super.onPostExecute(requestresult);
                if (issuelar == null) {
                    Log.i("bosmu", "Issuelar arrayi boş");
                } else {
                    SuresiGecenlerListesiAdapter adapter = new SuresiGecenlerListesiAdapter(getActivity(), issuelar);
                    setListAdapter(adapter);
                    //setRetainInstance(true);
                }
            }
        }

        private ArrayList<Issue> ParseJsonForIssue(String json) {
            //ArrayList<Issue> issuelar = new ArrayList<Issue>();
            if (json != null) {
                try {
                    JSONArray jsonObj = new JSONArray(json);
                    for (int i = 0; i < jsonObj.length(); i++) {
                        JSONObject issueObj = jsonObj.getJSONObject(i);
                        Issue issue = new Issue();
                        issue.setId(issueObj.getInt("id"));
                        issue.setTitle(issueObj.getString("title"));
                        issue.setContent(issueObj.getString("content"));
                        issue.setDueDate(issueObj.getString("dueDate"));
                        issue.setCreated_at(issueObj.getString("created_at"));
                        /*issue.setIsItDone(issueObj.getBoolean("isItDone"));
                        issue.setOverDue(issueObj.getBoolean("overDue"));*/
                        User user = new User();
                        user.setId(issueObj.getInt("user_id"));
                        issuelar.add(issue);
                    }
                } catch (JSONException e) {
                    Log.e("MYAPP", "unexpected JSON exception", e);
                    // Do something to recover ... or kill the app.
                }
            }
            return issuelar;
        }
    }
