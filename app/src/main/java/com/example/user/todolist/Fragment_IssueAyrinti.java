package com.example.user.todolist;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.text.format.DateFormat;
import android.text.method.KeyListener;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Calendar;

/**
 * Created by İlayda Şahiner on 17.07.2017.
 */

public class Fragment_IssueAyrinti extends Fragment {

    private static String urlAyrinti = "https://etucliquetodo.herokuapp.com/api/v1/users/1/issues/";
    private static String urlUpdate = "https://etucliquetodo.herokuapp.com/api/v1/users/1/issues/";
    Toolbar toolbar;
    static TextView tdate;
    static TextView ttime;
    String paramsForIssueUpdate="";

    int id;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        Bundle bundle = this.getArguments();
        id = bundle.getInt("id_of_clicked_issue", 0);
        urlAyrinti = urlAyrinti + id;
        urlUpdate = urlUpdate + id;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_issueayrinti, container, false);
        ColorDrawable colorDrawable = new ColorDrawable(Color.parseColor("#0d0a40"));
        ((AppCompatActivity) getActivity()).getSupportActionBar().setBackgroundDrawable(colorDrawable);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        new GetIssueAyrinti().execute();
    }

    private class GetIssueAyrinti extends AsyncTask<String, Void, String> {
        Issue issue;

        @Override
        protected String doInBackground(String... arg0) {

            WebRequest webreq = new WebRequest();
            String jsonStr = null;
            try {
                jsonStr = webreq.makeWebServiceCallGet(urlAyrinti);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.i(jsonStr, "!! Json");

            issue = ParseJsonForIssue(jsonStr);

            return "a";

        }

        @Override
        protected void onPostExecute(String requestresult) {
            super.onPostExecute(requestresult);
            if (issue == null) {
                Log.i("bosmu", "Issue nesnesi boş");
            } else {
                urlUpdate = "https://etucliquetodo.herokuapp.com/api/v1/users/1/issues/"+id;
                System.out.println("2 " + urlUpdate);
                final EditText title = (EditText) getView().findViewById(R.id.title);
                final EditText content = (EditText) getView().findViewById(R.id.content);
                TextView created_at = (TextView) getView().findViewById(R.id.created_at);

                ttime = (TextView) getView().findViewById(R.id.timetext);
                tdate = (TextView) getView().findViewById(R.id.datetext);
                final TextView timeInfo = (TextView) getView().findViewById(R.id.timeinfo);
                final TextView dateInfo = (TextView) getView().findViewById(R.id.dateinfo);
                final Button timebutton = (Button) getView().findViewById(R.id.time);
                final Button datebutton = (Button) getView().findViewById(R.id.date);

                final Button sil = (Button) getView().findViewById(R.id.sil);
                final Button duzenle = (Button) getView().findViewById(R.id.duzenle);
                final Button vazgec = (Button) getView().findViewById(R.id.vazgec);
                final Button guncelle = (Button) getView().findViewById(R.id.guncelle);
                final Button tamamlandi = (Button) getView().findViewById(R.id.tamamlandi);
                title.setTag(title.getKeyListener());
                title.setKeyListener(null);
                title.setText(issue.getTitle());
                content.setTag(content.getKeyListener());
                content.setKeyListener(null);
                content.setText(issue.getContent());
                created_at.setText(issue.getCreated_at());
                ttime.setText(issue.getDueDate().substring(11, 16));
                tdate.setText(issue.getDueDate().substring(0, 10));


                timebutton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        DialogFragment newFragment = new Fragment_IssueAyrinti.TimePickerFragment();
                        newFragment.show(getFragmentManager(), "timePicker");
                    }
                });

                datebutton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        DialogFragment newFragment = new Fragment_IssueAyrinti.DatePickerFragment();
                        newFragment.show(getFragmentManager(), "datePicker");
                    }
                });


                sil.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                        alertDialogBuilder.setTitle("Sil");
                        alertDialogBuilder.setMessage("Dosyayı silmek istediğinize emin misiniz?");
                        alertDialogBuilder.setPositiveButton("Vazgeç", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        alertDialogBuilder.setNegativeButton("Sil", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                new SilTask().execute();
                            }
                        });
                        final AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                            @Override
                            public void onShow(DialogInterface arg0) {
                                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.parseColor("#4C87F4"));
                                alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(Color.parseColor("#4C87F4"));
                            }
                        });
                        alertDialog.show();
                    }
                });

                duzenle.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        tamamlandi.setVisibility(View.GONE);
                        sil.setVisibility(View.GONE);
                        duzenle.setVisibility(View.GONE);
                        vazgec.setVisibility(View.VISIBLE);
                        datebutton.setVisibility(View.VISIBLE);
                        timebutton.setVisibility(View.VISIBLE);
                        timeInfo.setVisibility(View.VISIBLE);
                        dateInfo.setVisibility(View.VISIBLE);
                        guncelle.setVisibility(View.VISIBLE);

                        title.setKeyListener((KeyListener) title.getTag());
                        content.setKeyListener((KeyListener) content.getTag());



                    }
                });

                guncelle.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        String partitle = title.getText().toString();
                        String parcontent = content.getText().toString();
                        String time = ttime.getText().toString();
                        String date = tdate.getText().toString();
                        paramsForIssueUpdate = "title=" + partitle + "&" + "content=" +parcontent + "&" +"dueDate=" +date+" "+time;
                        System.out.println(paramsForIssueUpdate);
                        System.out.println("3 "+urlUpdate);
                        new GuncelleTask().execute();
                    }
                });

                vazgec.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        vazgec.setVisibility(View.GONE);
                        tamamlandi.setVisibility(View.VISIBLE);
                        sil.setVisibility(View.VISIBLE);
                        duzenle.setVisibility(View.VISIBLE);
                        datebutton.setVisibility(View.GONE);
                        timebutton.setVisibility(View.GONE);
                        timeInfo.setVisibility(View.GONE);
                        dateInfo.setVisibility(View.GONE);


                        title.setTag(title.getKeyListener());
                        title.setKeyListener(null);
                        title.setText(issue.getTitle());
                        content.setTag(title.getKeyListener());
                        content.setKeyListener(null);
                        content.setText(issue.getContent());
                        ttime.setText(issue.getDueDate().substring(11, 16));
                        tdate.setText(issue.getDueDate().substring(0, 10));
                    }
                });

                tamamlandi.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        new TamamlandiTask().execute();
                    }
                });


                urlAyrinti = "https://etucliquetodo.herokuapp.com/api/v1/users/1/issues/";

            }
        }
    }

    private class SilTask extends AsyncTask<String, Integer, String> {
        int response;
        @Override
        protected String doInBackground(String... sUrl) {

            WebRequest webreq = new WebRequest();
            response = 0;
            try {
                response = webreq.makeWebServiceCallDelete(urlUpdate);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.i("Sil için Response: ", "" + response);
            return "a";
        }


        @Override
        protected void onPostExecute(String result) {
            if (response == 204) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                alertDialogBuilder.setTitle("Sil");
                alertDialogBuilder.setMessage("Silme işleminiz başarılı bir şekilde gerçekleştirilmiştir.");
                alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                final AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface arg0) {
                        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.parseColor("#4C87F4"));
                    }
                });
                alertDialog.show();
                urlUpdate = "https://etucliquetodo.herokuapp.com/api/v1/users/1/issues/";
                getFragmentManager().popBackStack();
            }
            else {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                alertDialogBuilder.setTitle("Sil");
                alertDialogBuilder.setMessage("Silme işleminiz başarısız olmuştur");
                alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                final AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface arg0) {
                        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.parseColor("#4C87F4"));
                    }
                });
                alertDialog.show();
                urlUpdate = "https://etucliquetodo.herokuapp.com/api/v1/users/1/issues/";
                getFragmentManager().popBackStack();
            }
        }
    }

    private class TamamlandiTask extends AsyncTask<String, Integer, String> {

        @Override
        protected String doInBackground(String... sUrl) {
            System.out.println(urlUpdate);
            String paramsForTamamlandi="isItDone=true";
            WebRequest webreq = new WebRequest();
            String response = "";
            try {
                response = webreq.makeWebServiceCallUpdate(urlUpdate, paramsForTamamlandi);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.i("Update için Response: ", "" + response);
            return "a";
        }


        @Override
        protected void onPostExecute(String result) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
            alertDialogBuilder.setTitle("Tamamlandı");
            alertDialogBuilder.setMessage("Issue tamamlananlar listesine alınmıştır.");
            alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            final AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(DialogInterface arg0) {
                    alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.parseColor("#4C87F4"));
                }
            });
            alertDialog.show();
            urlUpdate = "https://etucliquetodo.herokuapp.com/api/v1/users/1/issues/";
            getFragmentManager().popBackStack();
        }
    }

    private class GuncelleTask extends AsyncTask<String, Integer, String> {

        @Override
        protected String doInBackground(String... sUrl) {
            System.out.println(urlUpdate);
            WebRequest webreq = new WebRequest();
            String response = "";
            try {
                response = webreq.makeWebServiceCallUpdate(urlUpdate,paramsForIssueUpdate);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.i("Update için Response: ", "" + response);
            return "a";
        }


        @Override
        protected void onPostExecute(String result) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
            alertDialogBuilder.setTitle("Güncelleme");
            alertDialogBuilder.setMessage("Güncelleme işleminiz başarılı bir şekilde gerçekleştirilmiştir.");
            alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            final AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(DialogInterface arg0) {
                    alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.parseColor("#4C87F4"));
                }
            });
            alertDialog.show();
            urlUpdate = "https://etucliquetodo.herokuapp.com/api/v1/users/1/issues/";
            getFragmentManager().popBackStack();
        }
    }

    private Issue ParseJsonForIssue(String json) {
        Issue issue = new Issue();
        if (json != null) {
            try {
                JSONObject issueObj = new JSONObject(json);
                issue.setId(issueObj.getInt("id"));
                issue.setTitle(issueObj.getString("title"));
                issue.setContent(issueObj.getString("content"));
                issue.setDueDate(issueObj.getString("dueDate"));
                issue.setCreated_at(issueObj.getString("created_at"));
                /*issue.setIsItDone(issueObj.getBoolean("isItDone"));
                  issue.setOverDue(issueObj.getBoolean("overDue"));*/
                User user = new User();
                user.setId(issueObj.getInt("user_id"));
            } catch (JSONException e) {
                Log.e("MYAPP", "unexpected JSON exception", e);
                // Do something to recover ... or kill the app.
            }
        }
        return issue;
    }


    public static class TimePickerFragment extends DialogFragment
            implements TimePickerDialog.OnTimeSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current time as the default values for the picker
            final Calendar c = Calendar.getInstance();
            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);

            // Create a new instance of TimePickerDialog and return it
            return new TimePickerDialog(getActivity(), this, hour, minute,
                    DateFormat.is24HourFormat(getActivity()));
        }

        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            String date = hourOfDay + ":" + String.valueOf(minute);
            System.out.println(date);
            if (hourOfDay < 10)
                date = "0" + hourOfDay + ":" + String.valueOf(minute);
            if (minute < 10)
                date = String.valueOf(hourOfDay) + ":0" + String.valueOf(minute);
            if(minute < 10 && hourOfDay<10)
                date = "0"+String.valueOf(hourOfDay) + ":0" + String.valueOf(minute);
            ttime.setText(date);

        }
    }

    public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            String date = String.valueOf(year) + "-" + String.valueOf(month + 1)
                    + "-" + String.valueOf(day);
            if (month < 10)
                date = String.valueOf(year) + "-0" + String.valueOf(month + 1)
                        + "-" + String.valueOf(day);
            if (day < 10)
                date = String.valueOf(year) + "-" + String.valueOf(month + 1)
                        + "-0" + String.valueOf(day);
            if (month < 10 && day < 10)
                date = String.valueOf(year) + "-0" + String.valueOf(month + 1)
                        + "-0" + String.valueOf(day);

            tdate.setText(date);
        }
    }
}
